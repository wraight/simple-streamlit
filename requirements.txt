streamlit==1.15.1
altair==4.2.0
itkdb==0.3.15
numpy==1.22.3
pandas==1.3.4
