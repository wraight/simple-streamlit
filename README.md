# simple streamlit

[Streamlit](https://www.streamlit.io) based app.

ITk PDB access via [itkdb](https://pypi.org/project/itkdb/).

__NB__ See requirements file for essential packages.

---

## Local running

Run streamlit in _streamlit_ directory. Specify main file, sub-pages will be picked up form _pages_ directory.

### Run streamlit

> streamlit run simpleApp.py --server.port=8501

arguments:
- set port: --server.port

### Access app

Open Browser with address "localhost:[port]"

---

## Build and run with Docker

Recommend downloading Docker desktop from [here](https://www.docker.com/products/docker-desktop/).

Run docker from top reporsitory directory.

### Building

> docker build -f dockerFiles/Dockerfile -t simple-app .

arguments:
- specify dockerfile path: -f
- specify image name: -t
- use current directory: .

### Running

> docker run -p 8501:8501 -v $(pwd)/streamlit/pages/:/code/streamlit/pages/ simple-app

arguments:
- specify port mapping from local resource to inside container: -p (outside:inside)
- specify memory mapping from local resource to inside container: -v (outside:inside)
- specify image name: final argument

### Access app

Open Browser with address "localhost:[port]"

---
