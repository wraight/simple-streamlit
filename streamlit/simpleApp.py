import streamlit as st
import pandas as pd
import numpy as np

st.title('Welcome')

st.write("An simple app demonstrate some [streamlit](https://streamlit.io) functionality.")

st.write("---")

st.write('Pages')
st.write('1. :chart_with_upwards_trend: __plotting__  - sample plot')
st.write('2. :clipboard: __script__ - run script')
st.write('3. :link: __embed__ - embed local and external html')
st.write('4. :customs: __itkdb__ - access ITk Production Database')

st.write("---")