import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX


### introduction
st.title(':customs: ITk Production Database interaction')
st.write('### Simple interaction with PDB')
st.write('useful packages')
st.write('- [pandas](https://pandas.pydata.org/docs/user_guide/index.html) for data wrangling')
st.write('- [itkdb](https://pypi.org/project/itkdb/) for PDB API')
st.write("---")

### input user info.
st.write("## User credentials")

ac1=st.text_input("first access token", type="password")
ac2=st.text_input("second access token", type="password")

### button method - problem with further interactions
# if st.button("Register!"):

### checkbox method - effective caching
loginCheck = st.checkbox('Ready to login')
if loginCheck:
    st.write("use tokens to get client")
    user = itkdb.core.User(accessCode1=ac1, accessCode2=ac2)
    try:
        user.authenticate()
        client = itkdb.Client(user=user)
        st.success("Logged on to PDB")
    except itkX.ResponseException:
        st.error("Cannot authenticate user credentials")
        st.stop()
else:
    st.stop()

st.write("---")

st.write("## ITk institutions")
instList=client.get('listInstitutions', json={} ).data
st.dataframe(instList)


st.write("---")

st.write("## Select institution")

selInst=st.selectbox("Select institution:",options=[i['name'] for i in instList])

st.write( next(i for i in instList if i['name']==selInst) )

