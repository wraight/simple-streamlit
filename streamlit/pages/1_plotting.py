import streamlit as st
import streamlit.components.v1 as components

import random
import pandas as pd
import altair as alt


### introduction
st.title(':chart_with_upwards_trend: Simple plotting example')
st.write('### Make a simple plot')
st.write('useful packages')
st.write('- [pandas](https://pandas.pydata.org/docs/user_guide/index.html) for data wrangling')
st.write('- [altair](https://altair-viz.github.io/user_guide/generated/toplevel/altair.Chart.html) for plotting')
st.write("---")

### settings
st.write("## User settings")

sampleSize=st.slider("Set sample size", min_value=1, max_value=50, value=20, step=1)
minVal, maxVal = st.select_slider("Select range of values", options=range(0,100,5), value=[0,20])

checkVal=st.checkbox("Set threhold (used for plot colour)", value=False)
if checkVal:
    threshold=st.slider("Threshold value", min_value=int(minVal), max_value=int(maxVal), value=int((maxVal-minVal)/2), step=1)
else:
    threshold=maxVal/2

dataList=[]
for i in range(0,sampleSize+1,1):
    dataList.append({'x':i,'y':random.randint(minVal, maxVal)})
    if dataList[-1]['y']<threshold:
        dataList[-1]['other']="below"
    else:
        dataList[-1]['other']="above"
st.write(f"got data! size: {len(dataList)}")

### pandas
st.write("## Wrangle data with _pandas_")

df_data=pd.DataFrame(dataList)
st.write(df_data)

### plotting
st.write("## Plotting with _altair_")
dataChart=alt.Chart(df_data).mark_circle(size=60).encode(
                x=alt.X('x:Q',title="x values"),
                y=alt.X('y:Q',title="y values"),
                color=alt.Color('other:N',title="threshold pass?"),
                tooltip=['x:Q', 'y:Q', 'color:N']
            ).properties(
                title="Values passing threshold: "+str(threshold),
                width=600
            ).interactive()
st.write(dataChart)
