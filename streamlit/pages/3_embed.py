import streamlit as st
import streamlit.components.v1 as components

import os
import json


### introduction
st.title(':link: Upload and embed')
st.write('### Translate _json_ file to show reference map of keys')
st.write("---")

### input file
st.write("## Upload file")
st.write("Upload _json_ or _txt_")

upFile=st.file_uploader("Upload file",type=['json','txt'])

if upFile==None:
    st.write("No file added")
else:
    st.write({"FileName":upFile.name,"FileType":upFile.type})
    bytes_data = upFile.getvalue()
    data = upFile.getvalue().decode('utf-8')  
    if "json" in upFile.type.lower():
        ### write tmp file
        with open("/tmp/tmpFile.json","w") as f: 
            f.write(data)
        st.write("### Input json")
        with open("/tmp/tmpFile.json","r") as f: 
            inFile = json.load(f)
            st.write(inFile)
    else:
        st.write(data)

### input file
st.write("## Embedding")
st.write("Embed html or link")

### html files
st.write("### local _html_ file")
htmlFiles=[f for f in os.listdir(os.getcwd()+"/htmls") if ".html" in f] 
st.write("html files:",htmlFiles)

if len(htmlFiles)>0:
    ### select spec
    htmlSel=st.selectbox("Select html file:",htmlFiles) 

    ### embed datapane report
    with open("./htmls/"+htmlSel, 'r') as f:
        components.html( f.read(), width=800, height=400 )
        # st.markdown(f.read(),unsafe_allow_html=True)
    
    ### download button
    with open("./htmls/"+htmlSel, 'rb') as file:
        st.download_button(
            label="Download html",
            data=file,
            file_name=htmlSel,
            mime="html"
          )

### webPage
st.write("### webPage example")

components.iframe("https://www.nationalgeographic.com/animals/mammals/facts/domestic-cat", width=800, height=400)

st.write("---")