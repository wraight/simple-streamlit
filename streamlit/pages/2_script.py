import streamlit as st

import os
import subprocess


### introduction
st.title(':clipboard: Using a script')
st.write('### Run a script and get terminal output')
st.write("---")

### scripts
st.write("## Select pre-set script")
pyFiles=[f for f in os.listdir(os.getcwd()+"/scripts") if ".py" in f] 
st.write(pyFiles)

if len(pyFiles)>0:
    ### select spec
    pySel=st.selectbox("Select script file:",pyFiles) 
else:
    st.write("no scripts found.")
    st.stop()


st.write("---")

st.write("## Run script")
st.write("Selected report to submit:")
st.write(f"> python3 {pySel}")

### set whole array again (avoid messing with indiviual arguments)
cmdArr=['python3',pySel]

if st.button("Run script!"):
    st.write("Running...")
    session = subprocess.Popen(cmdArr, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = session.communicate()

    if stdout:
        with open(os.path.join(os.getcwd()+"/outputs/script.out"),"wb") as f: 
            f.write(stdout)
        st.success("Output written!")
        # if st.checkbox("See full output"):
        #     st.write("Submitted "+stdout.decode("utf-8"))

    if stderr:
        st.error("Error "+stderr.decode("utf-8"))
    
### outputs
st.write("## Select output")
outFiles=[f for f in os.listdir(os.getcwd()+"/outputs")] 
st.write(outFiles)

if len(outFiles)>0:
    ### select spec
    outSel=st.selectbox("Select output file:",outFiles) 
    with open(os.getcwd()+"/outputs/"+outSel,"r") as f: 
        st.write(f.readlines())
else:
    st.write("no scripts found.")
    st.stop()